#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_botaoTestar_clicked();
    void kalditk();
    void cmusphinx();
    void htk();
    void init();
    void init_kaldi();
    int alertaErro();
    void valoresConf(int, QString, QString);
    void salvarConfig();
    int configuracao(int);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
