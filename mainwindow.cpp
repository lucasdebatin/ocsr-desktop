#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "kaldi.h"
#include "cmusphinx.h"
#include "htk.h"
#include <fstream>
#include <iostream>
#include <QMessageBox>
#include <thread>
#include "mythread.h"

static QString local = QString("/home/") + getenv("USER") + ROOT_DIR;
static QString localConfiguracao = local + "utils/configuracao.txt";;
static int qtd = 0;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->comboBoxBiblioteca->setCurrentIndex(configuracao(0));
    ui->comboBoxCorpusVoz->setCurrentIndex(configuracao(1));
    if (configuracao(2) == 1) { //Edita nome dos diretórios com base no computador do usuário
        init();
    }
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_botaoTestar_clicked() {
    salvarConfig();
    if (alertaErro() == 0) {
        ui->botaoTestar->setText("Gerando arquivo...");
        ui->botaoTestar->repaint();
        if (configuracao(0) == 0) { //Kaldi
            kalditk();
        } else if (configuracao(0) == 1) { //CMUSphinx
            cmusphinx();
        } else if (configuracao(0) == 2) { //HTK
            htk();
        }
    }
    ui->botaoTestar->setText("Testar");
    ui->botaoTestar->repaint();
}

void MainWindow::salvarConfig() {
    std::ofstream outFile;
    remove(localConfiguracao.toUtf8().constData());
    outFile.open(localConfiguracao.toUtf8().constData());
    outFile << ui->comboBoxBiblioteca->currentIndex() << "\n" << ui->comboBoxCorpusVoz->currentIndex() << "\n0";
    outFile.close();
}

int MainWindow::alertaErro() {
    int erro = 0;
    if ((configuracao(0) == 2) & (ui->comboBoxBiblioteca->currentIndex() == 2)) { //HTK
        if (qtd == 1) {
            QMessageBox msgBox;
            msgBox.setText("Dará um erro, necessário reiniciar o sistema!");
            msgBox.exec();
            erro = 1;
        } else {
            qtd = 1;
        }
    }
    return erro;
}

void MainWindow::kalditk() {
    QString corpusNome = "laps";
    if (configuracao(1) == 0) { //cbr
        corpusNome = "cbr";
    }
    QString comando;
    for (int i = 1; i < 5; i++) {
        if (i==4 && configuracao(1) == 0) {
            break;
        }
        comando = "rm ";
        comando += local + QString("utils/tests/") + corpusNome + QString("/kaldiTestPerformance") + std::to_string(i).c_str() + QString(".txt");
        system(comando.toUtf8().constData()); //limpa dados performance
        MyThread *mThread;
        mThread = new MyThread(this);
        connect(mThread, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
        comando = "top -n 1 | grep -E 'ocsr-desktop|nnet3-latgen|gmm-latgen' >> ";
        comando += local + QString("utils/tests/") + corpusNome + QString("/kaldiTestPerformance") + std::to_string(i).c_str() + QString(".txt");
        mThread->Command = comando;
        mThread->start();
        comando = QString("echo `date` > ");
        comando += local + QString("utils/tests/") + corpusNome + QString("/kaldiDateIni") + std::to_string(i).c_str() + QString(".txt");
        system(comando.toUtf8().constData()); //data inicial
        std::ofstream outFile;
        QString arquivo = local + "utils/tests/" + corpusNome + "/kaldi" + std::to_string(i).c_str() + ".txt";
        remove(arquivo.toUtf8().constData());
        kaldi k;
        k.test(configuracao(1), i);
        QString command = "";
        if (i == 1 && configuracao(1) == 1) {
            command = "cp " + local + "/utils/model/" + std::to_string(i).c_str() + "/kaldi/" + corpusNome + "/exp/nnet3/tdnn_sp/decode_test/scoring/test_output.txt " + arquivo;
        } else if ((i == 1 && configuracao(1) == 0) || (i == 2 && configuracao(1) == 1)) {
            command = "cp " + local + "/utils/model/" + std::to_string(i).c_str() + "/kaldi/" + corpusNome + "/exp/nnet3/lstm_sp/decode_test/scoring/test_output.txt " + arquivo;
        } else if ((i == 2 && configuracao(1) == 0) || (i == 3 && configuracao(1) == 1)) {
            command = "cp " + local + "/utils/model/" + std::to_string(i).c_str() + "/kaldi/" + corpusNome + "/exp/tri3b/decode/scoring/test_output.txt " + arquivo;
        } else {
            command = "cp " + local + "/utils/model/" + std::to_string(i).c_str() + "/kaldi/" + corpusNome + "/exp/tri1/decode/scoring/test_output.txt " + arquivo;
        }
        system(command.toUtf8().constData());
        comando = QString("echo `date` > ");
        comando += local + QString("utils/tests/") + corpusNome + QString("/kaldiDateFin") + std::to_string(i).c_str() + QString(".txt");
        system(comando.toUtf8().constData()); //data final
        mThread->Stop = true;
    }
    valoresConf(5, corpusNome, QString("kaldiTestPerformance"));
    QMessageBox msgBox;
    msgBox.setText("Arquivo de resultados do Kaldi gerado!");
    msgBox.exec();
}

void MainWindow::cmusphinx() {
    QString corpusNome = "laps";
    if (configuracao(1) == 0) { //cbr
        corpusNome = "cbr";
    }
    QString comando;
    for (int i = 1; i < 5; i++) {
        comando = "rm ";
        comando += local + QString("utils/tests/") + corpusNome + QString("/cmusphinxTestPerformance") + std::to_string(i).c_str() + QString(".txt");
        system(comando.toUtf8().constData()); //limpa dados performance
        MyThread *mThread;
        mThread = new MyThread(this);
        connect(mThread, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
        comando = "top -n 1 | grep 'ocsr-desktop' >> ";
        comando += local + QString("utils/tests/") + corpusNome + QString("/cmusphinxTestPerformance") + std::to_string(i).c_str() + QString(".txt");
        mThread->Command = comando;
        mThread->start();
        comando = QString("echo `date` > ");
        comando += local + QString("utils/tests/") + corpusNome + QString("/cmusphinxDateIni") + std::to_string(i).c_str() + QString(".txt");
        system(comando.toUtf8().constData()); //data inicial
        CMUSphinx sphinx;
        std::ofstream outFile;
        QString arquivo = local + QString("utils/tests/") + corpusNome + QString("/cmusphinx") + std::to_string(i).c_str() + QString(".txt");
        remove(arquivo.toUtf8().constData());
        outFile.open(arquivo.toUtf8().constData());
        outFile << sphinx.test(configuracao(1), i);
        outFile.close();
        comando = QString("echo `date` > ");
        comando += local + QString("utils/tests/") + corpusNome + QString("/cmusphinxDateFin") + std::to_string(i).c_str() + QString(".txt");
        system(comando.toUtf8().constData()); //data final
        mThread->Stop = true;
    }
    valoresConf(5, corpusNome, QString("cmusphinxTestPerformance"));
    QMessageBox msgBox;
    msgBox.setText("Arquivo de resultados do CMUSphinx gerado!");
    msgBox.exec();
}

void MainWindow::htk() {
    QString corpusNome = "laps";
    if (configuracao(1) == 0) { //cbr
        corpusNome = "cbr";
    }
    QString comando;
    int i = 1; //alterar manualmente, pois o HTK dá um erro se executar com o FOR
    comando = "rm ";
    comando += local + QString("utils/tests/") + corpusNome + QString("/htkTestPerformance") + std::to_string(i).c_str() + QString(".txt");
    system(comando.toUtf8().constData()); //limpa dados performance
    MyThread *mThread;
    mThread = new MyThread(this);
    connect(mThread, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
    comando = "top -n 1 | grep 'ocsr-desktop' >> ";
    comando += local + QString("utils/tests/") + corpusNome + QString("/htkTestPerformance") + std::to_string(i).c_str() + QString(".txt");
    mThread->Command = comando;
    mThread->start();
    comando = QString("echo `date` > ");
    comando += local + QString("utils/tests/") + corpusNome + QString("/htkDateIni") + std::to_string(i).c_str() + QString(".txt");
    system(comando.toUtf8().constData()); //data inicial
    HTK h;
    std::ofstream outFile;
    std::string  frases;
    std::ifstream inFile;
    QString arquivo = local + "utils/tests/" + corpusNome + "/htk" + std::to_string(i).c_str() + ".txt";
    inFile.open(arquivo.toUtf8().constData());
    h.test(configuracao(1), i);
    std::string linha;
    std::string linhaAux;
    QString linhaQT;
    QStringList list;
    if (inFile.is_open()) {
        int i = 0;
        while (getline(inFile, linha)) {
            linhaQT = linha.c_str();
            if (linhaQT.contains(".rec")) {
                if (i == 1) {
                    frases += linhaAux.replace(linhaAux.size()-1, 1, "");
                    frases += "\n";
                } else {
                    i = 1;
                }
                linhaQT = linha.c_str();
                list = linhaQT.split("/");
                for (QString aux : list) {
                    if (aux.contains("rec")) {
                        linhaAux = aux.toUtf8().constData();
                        break;
                    }
                }
                linhaAux = linhaAux.replace(linhaAux.size()-4, 4, "wav"); //Nome do arquivo
                linhaAux += " - ";
            } else if ((linhaQT != ".") & (linhaQT != "#!MLF!#")) {
                linhaQT = linha.c_str();
                list = linhaQT.split(" ");
                if ((list.at(2) != "sil") & (list.at(2) != "sp")) {
                    linhaAux += list.at(2).toUtf8().constData(); //frase
                    linhaAux += " ";
                }
            }
        }
        frases += linhaAux.replace(linhaAux.size()-1, 1, "");
        frases += "\n";
        inFile.close();
        remove(arquivo.toUtf8().constData());
        outFile.open(arquivo.toUtf8().constData());
        outFile << frases;
    }
    comando = QString("echo `date` > ");
    comando += local + QString("utils/tests/") + corpusNome + QString("/htkDateFin") + std::to_string(i).c_str() + QString(".txt");
    system(comando.toUtf8().constData()); //data final
    mThread->Stop = true;
    if (i == 4) {
        valoresConf(5, corpusNome, QString("htkTestPerformance"));
    }
    QMessageBox msgBox;
    msgBox.setText("Arquivo de resultados do HTK gerado!");
    msgBox.exec();
}

void MainWindow::valoresConf(int numQtd, QString corpusNome, QString bibiotecaNome) {
    for (int i = 1; i < numQtd; i++) {
        if (i==4 && configuracao(1) == 0) {
            break;
        }
        std::ofstream outFile;
        std::string  frases;
        std::ifstream inFile;
        QString arquivoAbrir = local + QString("utils/tests/") + corpusNome + QString("/") + bibiotecaNome + std::to_string(i).c_str() + QString(".txt");
        inFile.open(arquivoAbrir.toUtf8().constData());
        std::string linha;
        std::string linhaAux;
        QString linhaQT;
        QStringList list;
        QString subString;
        float maxCPU = 0;
        float minCPU = 999;
        float sumCPU = 0;
        int countCPU = 0;
        float maxMEM = 0;
        float minMEM = 999;
        float sumMEM = 0;
        int countMEM = 0;
        float valorAux = 0.00;
        if (inFile.is_open()) {
            while (getline(inFile, linha)) {
                linhaQT = linha.c_str();
                subString = linhaQT.mid(57,5).trimmed();
                subString = subString.replace(subString.size()-2, 1, ".");
                valorAux = subString.toFloat();
                sumCPU += valorAux;
                countCPU += 1;
                if (valorAux > maxCPU) {
                    maxCPU = valorAux;
                }
                if (valorAux < minCPU) {
                    minCPU = valorAux;
                }

                subString = linhaQT.mid(63,5).trimmed();
                subString = subString.replace(subString.size()-2, 1, ".");
                valorAux = subString.toFloat();
                sumMEM += valorAux;
                countMEM += 1;
                if (valorAux > maxMEM) {
                    maxMEM = valorAux;
                }
                if (valorAux < minMEM) {
                    minMEM = valorAux;
                }
            }
            float medCPU = 0;
            if (countCPU > 0) {
                medCPU = sumCPU / countCPU;
            }
            float medMEM = 0;
            if (countMEM > 0) {
                medMEM = sumMEM / countMEM;
            }
            frases = "Max Mem: ";
            frases += std::to_string(maxMEM);
            frases += " - Min Mem: ";
            frases += std::to_string(minMEM);
            frases += " - Med Men: ";
            frases += std::to_string(medMEM);
            frases += "\n";
            frases += "Max CPU: ";
            frases += std::to_string(maxCPU);
            frases += " - Min CPU: ";
            frases += std::to_string(minCPU);
            frases += " - Med CPU: ";
            frases += std::to_string(medCPU);
            frases += "\n";
            inFile.close();
        }
        QString arquivoSalvar = local + QString("utils/tests/") + corpusNome + QString("/") + bibiotecaNome + std::to_string(i).c_str() + QString("_VALOR.txt");
        remove(arquivoSalvar.toUtf8().constData());
        outFile.open(arquivoSalvar.toUtf8().constData());
        outFile << frases;
    }
}

void MainWindow::init() {
    std::ofstream outFile;
    remove(localConfiguracao.toUtf8().constData());
    outFile.open(localConfiguracao.toUtf8().constData());
    outFile << ui->comboBoxBiblioteca->currentIndex() << "\n" << ui->comboBoxCorpusVoz->currentIndex() << "\n0";
    outFile.close();

    for (int i = 1; i < 5; i++) {
        QString command = "cp " + local + "utils/model/" + std::to_string(i).c_str() + "/htk/laps/mfc_test.list_BAK " + local + "utils/model/" + std::to_string(i).c_str() + "/htk/laps/mfc_test.list";
        system(command.toUtf8().constData());
        command = "cp " + local + "utils/model/" + std::to_string(i).c_str() + "/htk/cbr/mfc_test.list_BAK " + local + "utils/model/" + std::to_string(i).c_str() + "/htk/cbr/mfc_test.list";
        system(command.toUtf8().constData());

        QString localDir;
        std::string linha;
        std::string arquivos = "";
        std::ifstream inFile;
        inFile.open(QString(local+QString("utils/model/") + std::to_string(i).c_str() + QString("/htk/laps/mfc_test.list")).toUtf8().constData());
        if (inFile.is_open()) {
            arquivos = "";
            while (getline(inFile, linha)) {
                localDir = local+QString("utils/model/") + std::to_string(i).c_str() + QString("/htk/laps/corpus");
                arquivos += linha.replace(0, 1, localDir.toUtf8().constData()) + "\n";
            }
            outFile.open(QString(local+QString("utils/model/") + std::to_string(i).c_str() + QString("/htk/laps/mfc_test.list")).toUtf8().constData());
            outFile << arquivos;
            outFile.close();
            inFile.close();
        }
        inFile.open(QString(local+QString("utils/model/") + std::to_string(i).c_str() + QString("/htk/cbr/mfc_test.list")).toUtf8().constData());
        if (inFile.is_open()) {
            arquivos = "";
            while (getline(inFile, linha)) {
                localDir = local+QString("utils/model/") + std::to_string(i).c_str() + QString("/htk/cbr/corpus");
                arquivos += linha.replace(0, 1, localDir.toUtf8().constData()) + "\n";
            }
            outFile.open(QString(local+QString("utils/model/") + std::to_string(i).c_str() + QString("/htk/cbr/mfc_test.list")).toUtf8().constData());
            outFile << arquivos;
            outFile.close();
            inFile.close();
        }
    }
    init_kaldi();
    QMessageBox msgBox;
    msgBox.setText("Diretórios de arquivos alterados!");
    msgBox.exec();
}

void MainWindow::init_kaldi() {
    QString command = "";
    QString corpusNome[] = {"cbr", "laps"};
    QString nomeArquivosCBR[] = {"feats", "cmvn", "split1/1/feats", "split1/1/cmvn", "split1/1/wav"};
    QString nomeArquivosLAPS[] = {"feats", "cmvn", "split3/1/feats", "split3/1/cmvn", "split3/1/wav", "split3/2/feats", "split3/2/cmvn", "split3/2/wav", "split3/3/feats", "split3/3/cmvn", "split3/3/wav"};
    std::ofstream outFile;
    QString localDir;
    std::string linha;
    std::string arquivos = "";
    std::ifstream inFile;
    QString nomeTest = "";
    for (int c = 1; c < 5; c++) {
        for(int i = 0; i < 2; ++i) {
            if (c == 4 && i == 0) {
                break;
            }
            nomeTest = "test";
            if (c == 1 || (c == 2 && i == 1)) { //Redes neurais
                nomeTest = "test_hires";
                command = "cp " + local + "utils/model/" + std::to_string(c).c_str() + "/kaldi/"+corpusNome[i]+"/exp/nnet3/ivectors_test_hires/ivector_online.scp_BAK " + local + "utils/model/" + std::to_string(c).c_str() + "/kaldi/"+corpusNome[i]+"/exp/nnet3/ivectors_test_hires/ivector_online.scp";
                system(command.toUtf8().constData());
                inFile.open(QString(local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/"+corpusNome[i]+"/exp/nnet3/ivectors_test_hires/ivector_online.scp")).toUtf8().constData());
                if (inFile.is_open()) {
                    arquivos = "";
                    while (getline(inFile, linha)) {
                        localDir = local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/"+corpusNome[i]);
                        arquivos += linha.replace(linha.find("x-x"), 3, localDir.toUtf8().constData()) + "\n";
                    }
                    outFile.open(QString(local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/"+corpusNome[i]+"/exp/nnet3/ivectors_test_hires/ivector_online.scp")).toUtf8().constData());
                    outFile << arquivos;
                    outFile.close();
                    inFile.close();
                }
            }
            if (i == 0) { //CBR
                for (int l = 0; l < 4; l++) {
                    QString command = "cp " + local + "utils/model/" + std::to_string(c).c_str() + "/kaldi/cbr/data/" + nomeTest + "/"+nomeArquivosCBR[l]+".scp_BAK " + local + "utils/model/" + std::to_string(c).c_str() + "/kaldi/cbr/data/" + nomeTest + "/"+nomeArquivosCBR[l]+".scp";
                    system(command.toUtf8().constData());
                    inFile.open(QString(local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/cbr/data/" + nomeTest + "/"+nomeArquivosCBR[l]+".scp")).toUtf8().constData());
                    if (inFile.is_open()) {
                        arquivos = "";
                        while (getline(inFile, linha)) {
                            if (l == 4) { //split1/1/wav
                                localDir = local+QString("utils/tests/cbr/corpus");
                            } else {
                                localDir = local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/cbr");
                            }
                            arquivos += linha.replace(linha.find("x-x"), 3, localDir.toUtf8().constData()) + "\n";
                        }
                        outFile.open(QString(local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/cbr/data/" + nomeTest + "/"+nomeArquivosCBR[l]+".scp")).toUtf8().constData());
                        outFile << arquivos;
                        outFile.close();
                        inFile.close();
                    }
                }
            } else { //LAPS
                for (int l = 0; l < 10; l++) {
                    QString command = "cp " + local + "utils/model/" + std::to_string(c).c_str() + "/kaldi/laps/data/" + nomeTest + "/"+nomeArquivosLAPS[l]+".scp_BAK " + local + "utils/model/" + std::to_string(c).c_str() + "/kaldi/laps/data/" + nomeTest + "/"+nomeArquivosLAPS[l]+".scp";
                    system(command.toUtf8().constData());
                    inFile.open(QString(local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/laps/data/" + nomeTest + "/"+nomeArquivosLAPS[l]+".scp")).toUtf8().constData());
                    if (inFile.is_open()) {
                        arquivos = "";
                        while (getline(inFile, linha)) {
                            if (l == 4) { //split1/1/wav
                                localDir = local+QString("utils/tests/laps/corpus");
                            } else {
                                localDir = local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/laps");
                            }
                            arquivos += linha.replace(linha.find("x-x"), 3, localDir.toUtf8().constData()) + "\n";
                        }
                        outFile.open(QString(local+QString("utils/model/") + std::to_string(c).c_str() + QString("/kaldi/laps/data/" + nomeTest + "/"+nomeArquivosLAPS[l]+".scp")).toUtf8().constData());
                        outFile << arquivos;
                        outFile.close();
                        inFile.close();
                    }
                }
            }
        }
    }
}

int MainWindow::configuracao(int configIndex) { //configIndex -> 0 = biblioteca - 1 = corpus
    std::string config;
    std::ifstream inFile (localConfiguracao.toUtf8().constData());
    if (inFile.is_open()) {
        int i = 0;
        while (getline(inFile, config)) {
            if (i == configIndex) {
                break;
            }
            i ++;
        }
        inFile.close();
    }
    return atoi(config.c_str());
}
