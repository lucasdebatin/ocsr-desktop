# This file was created by the command:
# steps/nnet3/xconfig_to_configs.py --xconfig-file exp/nnet3/lstm_sp/configs/network.xconfig --config-dir exp/nnet3/lstm_sp/configs
# It contains the entire neural network, but with those
# components that would normally require fixed vectors/matrices
# read from disk, replaced with random initialization
# (this applies to the LDA-like transform and the
# presoftmax-prior-scale, if applicable).  This file
# is used only to work out the left-context and right-context
# of the network.

input-node name=ivector dim=100
input-node name=input dim=40
component name=lda type=FixedAffineComponent input-dim=300 output-dim=300
component-node name=lda component=lda input=Append(Offset(input, -2), Offset(input, -1), input, Offset(input, 1), Offset(input, 2), ReplaceIndex(ivector, t, 0))
##  Begin LTSM layer 'lstm1-forward'
# Gate control: contains W_i, W_f, W_c and W_o matrices as blocks.
component name=lstm1-forward.W_all type=NaturalGradientAffineComponent input-dim=332 output-dim=1024  max-change=1.5 
# The core LSTM nonlinearity, implemented as a single component.
# Input = (i_part, f_part, c_part, o_part, c_{t-1}), output = (c_t, m_t)
# See cu-math.h:ComputeLstmNonlinearity() for details.
component name=lstm1-forward.lstm_nonlin type=LstmNonlinearityComponent cell-dim=256 use-dropout=false  max-change=0.75 
# Component for backprop truncation, to avoid gradient blowup in long training examples.
component name=lstm1-forward.cr_trunc type=BackpropTruncationComponent dim=288 clipping-threshold=30.0 zeroing-threshold=15.0 zeroing-interval=20 recurrence-interval=1 scale=0.75
# Component specific to 'projected' LSTM (LSTMP), contains both recurrent
# and non-recurrent projections
component name=lstm1-forward.W_rp type=NaturalGradientAffineComponent input-dim=256 output-dim=64  max-change=1.5 
###  Nodes for the components above.
component-node name=lstm1-forward.W_all component=lstm1-forward.W_all input=Append(lda, IfDefined(Offset(lstm1-forward.r_trunc, -1)))
component-node name=lstm1-forward.lstm_nonlin component=lstm1-forward.lstm_nonlin input=Append(lstm1-forward.W_all, IfDefined(Offset(lstm1-forward.c_trunc, -1)))
dim-range-node name=lstm1-forward.c input-node=lstm1-forward.lstm_nonlin dim-offset=0 dim=256
dim-range-node name=lstm1-forward.m input-node=lstm1-forward.lstm_nonlin dim-offset=256 dim=256
# lstm1-forward.rp is the output node of this layer (if we're not including batchnorm)
component-node name=lstm1-forward.rp component=lstm1-forward.W_rp input=lstm1-forward.m
dim-range-node name=lstm1-forward.r input-node=lstm1-forward.rp dim-offset=0 dim=32
# Note: it's not 100% efficient that we have to stitch the c
# and r back together to truncate them but it probably
# makes the deriv truncation more accurate .
component-node name=lstm1-forward.cr_trunc component=lstm1-forward.cr_trunc input=Append(lstm1-forward.c, lstm1-forward.r)
dim-range-node name=lstm1-forward.c_trunc input-node=lstm1-forward.cr_trunc dim-offset=0 dim=256
dim-range-node name=lstm1-forward.r_trunc input-node=lstm1-forward.cr_trunc dim-offset=256 dim=32
### End LSTM Layer 'lstm1-forward'
##  Begin LTSM layer 'lstm1-backward'
# Gate control: contains W_i, W_f, W_c and W_o matrices as blocks.
component name=lstm1-backward.W_all type=NaturalGradientAffineComponent input-dim=332 output-dim=1024  max-change=1.5 
# The core LSTM nonlinearity, implemented as a single component.
# Input = (i_part, f_part, c_part, o_part, c_{t-1}), output = (c_t, m_t)
# See cu-math.h:ComputeLstmNonlinearity() for details.
component name=lstm1-backward.lstm_nonlin type=LstmNonlinearityComponent cell-dim=256 use-dropout=false  max-change=0.75 
# Component for backprop truncation, to avoid gradient blowup in long training examples.
component name=lstm1-backward.cr_trunc type=BackpropTruncationComponent dim=288 clipping-threshold=30.0 zeroing-threshold=15.0 zeroing-interval=20 recurrence-interval=1 scale=0.75
# Component specific to 'projected' LSTM (LSTMP), contains both recurrent
# and non-recurrent projections
component name=lstm1-backward.W_rp type=NaturalGradientAffineComponent input-dim=256 output-dim=64  max-change=1.5 
###  Nodes for the components above.
component-node name=lstm1-backward.W_all component=lstm1-backward.W_all input=Append(lda, IfDefined(Offset(lstm1-backward.r_trunc, 1)))
component-node name=lstm1-backward.lstm_nonlin component=lstm1-backward.lstm_nonlin input=Append(lstm1-backward.W_all, IfDefined(Offset(lstm1-backward.c_trunc, 1)))
dim-range-node name=lstm1-backward.c input-node=lstm1-backward.lstm_nonlin dim-offset=0 dim=256
dim-range-node name=lstm1-backward.m input-node=lstm1-backward.lstm_nonlin dim-offset=256 dim=256
# lstm1-backward.rp is the output node of this layer (if we're not including batchnorm)
component-node name=lstm1-backward.rp component=lstm1-backward.W_rp input=lstm1-backward.m
dim-range-node name=lstm1-backward.r input-node=lstm1-backward.rp dim-offset=0 dim=32
# Note: it's not 100% efficient that we have to stitch the c
# and r back together to truncate them but it probably
# makes the deriv truncation more accurate .
component-node name=lstm1-backward.cr_trunc component=lstm1-backward.cr_trunc input=Append(lstm1-backward.c, lstm1-backward.r)
dim-range-node name=lstm1-backward.c_trunc input-node=lstm1-backward.cr_trunc dim-offset=0 dim=256
dim-range-node name=lstm1-backward.r_trunc input-node=lstm1-backward.cr_trunc dim-offset=256 dim=32
### End LSTM Layer 'lstm1-backward'
component name=output.affine type=NaturalGradientAffineComponent input-dim=64 output-dim=768  max-change=0.3 param-stddev=0.0 bias-stddev=0.0
component-node name=output.affine component=output.affine input=lstm1-backward.rp
component name=output.log-softmax type=LogSoftmaxComponent dim=768
component-node name=output.log-softmax component=output.log-softmax input=output.affine
output-node name=output input=output.log-softmax objective=linear
