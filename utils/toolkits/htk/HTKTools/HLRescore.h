#ifndef _HLRESCORE_H_
#define _HLRESCORE_H_

#ifdef __cplusplus
extern "C" {
#endif

int mainHLRescore(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif  /* _HLRESCORE_H_ */
