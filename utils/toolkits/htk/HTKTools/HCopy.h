#ifndef _HCOPY_H_
#define _HCOPY_H_

#ifdef __cplusplus
extern "C" {
#endif

int mainCopy(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif  /* _HCOPY_H_ */
