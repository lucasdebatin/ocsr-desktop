#-------------------------------------------------
#
# Project created by QtCreator 2018-11-12T14:43:37
#
#-------------------------------------------------

QT                                      += core gui
QT                                      += multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT    += widgets

TARGET      = ocsr-desktop
TEMPLATE    = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += ROOT_DIR=\\\"/Documents/Workspace/qtcreator/ocsr-desktop/\\\"

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += utils/toolkits/CMUSphinx/pocketsphinx/include
INCLUDEPATH += utils/toolkits/CMUSphinx/sphinxbase/include

INCLUDEPATH += utils/toolkits/htk
INCLUDEPATH += utils/toolkits/htk/HTKTools
INCLUDEPATH += utils/toolkits/htk/HTKLib
INCLUDEPATH += utils/toolkits/htk/HLMTools
INCLUDEPATH += utils/toolkits/htk/HLMLib
INCLUDEPATH += utils/toolkits/htk/HTKLVRec
LIBS        += -Lutils/toolkits/htk/HTKLib
LIBS        += -L/usr/X11R6/lib

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        cmusphinx.cpp \
        htk.cpp \
        kaldi.cpp \
        mythread.cpp \
        utils/toolkits/htk/HTKLib/esig_asc.c \
        utils/toolkits/htk/HTKLib/esig_edr.c \
        utils/toolkits/htk/HTKLib/esig_nat.c \
        utils/toolkits/htk/HTKLib/esignal.c \
        utils/toolkits/htk/HTKLib/HAdapt.c \
        utils/toolkits/htk/HTKLib/HLabel.c \
        utils/toolkits/htk/HTKLib/HAudio.c \
        utils/toolkits/htk/HTKLib/HDict.c \
        utils/toolkits/htk/HTKLib/HFB.c \
        utils/toolkits/htk/HTKLib/HMath.c \
        utils/toolkits/htk/HTKLib/HMem.c \
        utils/toolkits/htk/HTKLib/HModel.c \
        utils/toolkits/htk/HTKLib/HParm.c \
        utils/toolkits/htk/HTKLib/HRec.c \
        utils/toolkits/htk/HTKLib/HShell.c \
        utils/toolkits/htk/HTKLib/HSigP.c \
        utils/toolkits/htk/HTKLib/HTrain.c \
        utils/toolkits/htk/HTKLib/HUtil.c \
        utils/toolkits/htk/HTKLib/HVQ.c \
        utils/toolkits/htk/HTKLib/HWave.c \
        utils/toolkits/htk/HTKLib/HNet.c \
        utils/toolkits/htk/HTKLib/HMap.c \
        utils/toolkits/htk/HTKLib/HLM.c \
        utils/toolkits/htk/HTKLib/HLat.c \
        utils/toolkits/htk/HTKTools/HVite.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/pocketsphinx.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/acmod.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/dict.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/dict2pid.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/phone_loop_search.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/kws_search.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/fsg_search.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ngram_search.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/allphone_search.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/bin_mdef.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ps_lattice.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/tmat.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ms_mgau.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ptm_mgau.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/s2_semi_mgau.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ps_mllr.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/hmm.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/kws_detections.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/fsg_history.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/fsg_lextree.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ngram_search_fwdtree.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ngram_search_fwdflat.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/blkarray_list.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/mdef.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/vector.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ms_gauden.c \
        utils/toolkits/CMUSphinx/pocketsphinx/src/libpocketsphinx/ms_senone.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/cmd_ln.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/err.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/mmio.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/case.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/filename.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/dtoa.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/bio.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/pio.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/listelem_alloc.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/hash_table.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/logmath.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/bitarr.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/genrand.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/priority_queue.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/ckd_alloc.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/strfuncs.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/profile.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/glist.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/util/bitvec.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/ngram_model_set.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/ngram_model.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/ngram_model_trie.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/lm_trie.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/lm_trie_quant.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/ngrams_raw.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/jsgf.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/jsgf_scanner.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/jsgf_parser.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/lm/fsg_model.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_interface.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_sigproc.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_noise.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_prespch_buf.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_warp_inverse_linear.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_warp_affine.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_warp_piecewise_linear.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/fe/fe_warp.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/feat/feat.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/feat/cmn.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/feat/cmn_live.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/feat/agc.c \
        utils/toolkits/CMUSphinx/sphinxbase/src/libsphinxbase/feat/lda.c

HEADERS += \
        mainwindow.h \
        cmusphinx.h \
        htk.h \
        kaldi.h \
        audiorecorder.h \
        mythread.h \
        utils/toolkits/htk/HTKTools/HVite.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
