#include "mythread.h"
#include <QDebug>

MyThread::MyThread(QObject *parent, bool b) : QThread(parent), Stop(b) {}

// run() will be called when a thread starts
void MyThread::run()
{
    while(true)
    {
        if(this->Stop)
            break;

        system(this->Command.toUtf8().constData()); //coleta processamento
        // slowdown the count change, msec
        this->msleep(500);
    }
}
