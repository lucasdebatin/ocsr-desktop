#ifndef HTK_H
#define HTK_H
#include <string>
#include <HVite.h>
#include <HLRescore.h>

extern "C"

class HTK
{
public:
    HTK();
    void test(int, int);
};

#endif // HTK_H
