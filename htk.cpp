#include "htk.h"
#include <iostream>
#include <QDir>
#include <QMessageBox>

static QString local = QString("/home/") + getenv("USER") + ROOT_DIR;

HTK::HTK() {}

void HTK::test(int corpus, int configuracao) {
    QString corpusNome = "laps";
    if (corpus == 0) { //cbr
        corpusNome = "cbr";
    }
    QString htkTXT = local+QString("utils/tests/")+corpusNome+QString("/htk") + std::to_string(configuracao).c_str() + QString(".txt");
    QByteArray htkTXTba = htkTXT.toLocal8Bit();
    QString macros = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/hmms22/macros");
    QByteArray macrosba = macros.toLocal8Bit();
    QString hmmdefs = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/hmms22/hmmdefs");
    QByteArray hmmdefsba = hmmdefs.toLocal8Bit();
    QString mfcTEST = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/mfc_test.list");
    QByteArray mfcTESTba = mfcTEST.toLocal8Bit();
    QString lattices = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/lattices/");
    QByteArray latticesba = lattices.toLocal8Bit();
    QString wdnet = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/network");
    QByteArray wdnetba = wdnet.toLocal8Bit();
    QString dictionaryDIC = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/dictionary.dic");
    QByteArray dictionaryDICba = dictionaryDIC.toLocal8Bit();
    QString trifone = local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/htk/")+corpusNome+QString("/tiedlist");
    QByteArray trifoneba = trifone.toLocal8Bit();
    char *args[] =
    {
        "HVite",
        "-T", "1",
        "-n", "3",
        "-i", htkTXTba.data(),
        "-H", macrosba.data(),
        "-H", hmmdefsba.data(),
        "-S", mfcTESTba.data(),
        "-z", "lat",
        "-l", latticesba.data(),
        "-w", wdnetba.data(),
        dictionaryDICba.data(),
        trifoneba.data()
    };
    mainHVite(21, args);
}
