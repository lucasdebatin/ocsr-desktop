#include "kaldi.h"
#include <iostream>
#include <QDir>
#include <QMessageBox>
#include <QProcess>

static QString local = QString("/home/") + getenv("USER") + ROOT_DIR;

kaldi::kaldi() {}

void kaldi::test(int corpus, int configuracao) {
    QString corpusNome = "laps";
    if (corpus == 0) { //cbr
        corpusNome = "cbr";
    }
    QString caminhoSRC = local + "utils/toolkits/kaldi/src/";
    QString caminhoCorpus = local + "utils/model/" + std::to_string(configuracao).c_str() + "/kaldi/" + corpusNome + "/";
    QString command = "";
    if (corpus == 0 && configuracao == 1) {
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=50 --extra-right-context=50 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/lstm_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split1/1/utt2spk scp:" + caminhoCorpus + "data/test_hires/split1/1/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split1/1/feats.scp ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/lat.1.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/scoring/test_output.txt";
        system(command.toUtf8().constData());

        //Chain
        //command = "cat " + caminhoCorpus + "exp/chain/lstm_sp_bi/decode_test/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang_chain/words.txt > " + caminhoCorpus + "exp/chain/lstm_sp_bi/decode_test/scoring/test_output.txt";
        //system(command.toUtf8().constData());

        //Normal
        command = "cat " + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/scoring/test_output.txt";
        system(command.toUtf8().constData());
    } else if (corpus == 0 && configuracao == 2) {
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --determinize-lattice=false --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/tri3b/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split1/1/utt2spk scp:" + caminhoCorpus + "data/test/split1/1/cmvn.scp scp:" + caminhoCorpus + "data/test/split1/1/feats.scp ark:- | " + caminhoSRC + "featbin/splice-feats --left-context=2 --right-context=2 ark:- ark:- | " + caminhoSRC + "featbin/transform-feats " + caminhoCorpus + "exp/tri3b/final.mat ark:- ark:- | " + caminhoSRC + "featbin/transform-feats --utt2spk=ark:" + caminhoCorpus + "data/test/split1/1/utt2spk ark:" + caminhoCorpus + "exp/tri3b/decode/trans.1 ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri3b/decode/lat.tmp.1.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/tri3b/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());

        command = "cat " + caminhoCorpus + "exp/tri3b/decode/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/tri3b/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());
    } else if (corpus == 0 && configuracao == 3) {
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri1/graph/words.txt " + caminhoCorpus + "exp/tri1/final.mdl " + caminhoCorpus + "exp/tri1/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split1/1/utt2spk scp:" + caminhoCorpus + "data/test/split1/1/cmvn.scp scp:" + caminhoCorpus + "data/test/split1/1/feats.scp ark:- | " + caminhoSRC + "featbin/add-deltas  ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri1/decode/lat.1.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/tri1/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());

        command = "cat " + caminhoCorpus + "exp/tri1/decode/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/tri1/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());
    } else if (corpus == 1 && configuracao == 1) {
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=0 --extra-right-context=0 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/tdnn_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split3/1/utt2spk scp:" + caminhoCorpus + "data/test_hires/split3/1/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split3/1/feats.scp ark:- |' 'ark:|" + caminhoSRC + "latbin/lattice-scale --acoustic-scale=2.0 ark:- ark:- | gzip -c >" + caminhoCorpus + "exp/nnet3/tdnn_sp/decode_test/lat.1.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=0 --extra-right-context=0 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/tdnn_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split3/2/utt2spk scp:" + caminhoCorpus + "data/test_hires/split3/2/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split3/2/feats.scp ark:- |' 'ark:|" + caminhoSRC + "latbin/lattice-scale --acoustic-scale=2.0 ark:- ark:- | gzip -c >" + caminhoCorpus + "exp/nnet3/tdnn_sp/decode_test/lat.2.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=0 --extra-right-context=0 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/tdnn_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split3/3/utt2spk scp:" + caminhoCorpus + "data/test_hires/split3/3/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split3/3/feats.scp ark:- |' 'ark:|" + caminhoSRC + "latbin/lattice-scale --acoustic-scale=2.0 ark:- ark:- | gzip -c >" + caminhoCorpus + "exp/nnet3/tdnn_sp/decode_test/lat.3.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/nnet3/tdnn_sp/decode_test/scoring/test_output.txt";
        system(command.toUtf8().constData());

        command = "cat " + caminhoCorpus + "exp/nnet3/tdnn_sp/decode_test/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/nnet3/tdnn_sp/decode_test/scoring/test_output.txt";
        system(command.toUtf8().constData());
    } else if (corpus == 1 && configuracao == 2) {
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=50 --extra-right-context=50 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/lstm_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split3/1/utt2spk scp:" + caminhoCorpus + "data/test_hires/split3/1/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split3/1/feats.scp ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/lat.1.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=50 --extra-right-context=50 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/lstm_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split3/2/utt2spk scp:" + caminhoCorpus + "data/test_hires/split3/2/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split3/2/feats.scp ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/lat.2.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "nnet3bin/nnet3-latgen-faster --online-ivectors=scp:" + caminhoCorpus + "exp/nnet3/ivectors_test_hires/ivector_online.scp --online-ivector-period=10 --frames-per-chunk=50 --extra-left-context=50 --extra-right-context=50 --extra-left-context-initial=-1 --extra-right-context-final=-1 --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/nnet3/lstm_sp/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:" + caminhoCorpus + "data/test_hires/split3/3/utt2spk scp:" + caminhoCorpus + "data/test_hires/split3/3/cmvn.scp scp:" + caminhoCorpus + "data/test_hires/split3/3/feats.scp ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/lat.3.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/scoring/test_output.txt";
        system(command.toUtf8().constData());

        command = "cat " + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/nnet3/lstm_sp/decode_test/scoring/test_output.txt";
        system(command.toUtf8().constData());
    } else if (corpus == 1 && configuracao == 3) {
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --determinize-lattice=false --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/tri3b/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split3/1/utt2spk scp:" + caminhoCorpus + "data/test/split3/1/cmvn.scp scp:" + caminhoCorpus + "data/test/split3/1/feats.scp ark:- | " + caminhoSRC + "featbin/splice-feats --left-context=2 --right-context=2 ark:- ark:- | " + caminhoSRC + "featbin/transform-feats " + caminhoCorpus + "exp/tri3b/final.mat ark:- ark:- | " + caminhoSRC + "featbin/transform-feats --utt2spk=ark:" + caminhoCorpus + "data/test/split3/1/utt2spk ark:" + caminhoCorpus + "exp/tri3b/decode/trans.1 ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri3b/decode/lat.tmp.1.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --determinize-lattice=false --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/tri3b/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split3/2/utt2spk scp:" + caminhoCorpus + "data/test/split3/2/cmvn.scp scp:" + caminhoCorpus + "data/test/split3/2/feats.scp ark:- | " + caminhoSRC + "featbin/splice-feats --left-context=2 --right-context=2 ark:- ark:- | " + caminhoSRC + "featbin/transform-feats " + caminhoCorpus + "exp/tri3b/final.mat ark:- ark:- | " + caminhoSRC + "featbin/transform-feats --utt2spk=ark:" + caminhoCorpus + "data/test/split3/2/utt2spk ark:" + caminhoCorpus + "exp/tri3b/decode/trans.2 ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri3b/decode/lat.tmp.2.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --determinize-lattice=false --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri3b/graph/words.txt " + caminhoCorpus + "exp/tri3b/final.mdl " + caminhoCorpus + "exp/tri3b/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split3/3/utt2spk scp:" + caminhoCorpus + "data/test/split3/3/cmvn.scp scp:" + caminhoCorpus + "data/test/split3/3/feats.scp ark:- | " + caminhoSRC + "featbin/splice-feats --left-context=2 --right-context=2 ark:- ark:- | " + caminhoSRC + "featbin/transform-feats " + caminhoCorpus + "exp/tri3b/final.mat ark:- ark:- | " + caminhoSRC + "featbin/transform-feats --utt2spk=ark:" + caminhoCorpus + "data/test/split3/3/utt2spk ark:" + caminhoCorpus + "exp/tri3b/decode/trans.3 ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri3b/decode/lat.tmp.3.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/tri3b/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());

        command = "cat " + caminhoCorpus + "exp/tri3b/decode/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/tri3b/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());
    } else if (corpus == 1 && configuracao == 4) {
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri1/graph/words.txt " + caminhoCorpus + "exp/tri1/final.mdl " + caminhoCorpus + "exp/tri1/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split3/1/utt2spk scp:" + caminhoCorpus + "data/test/split3/1/cmvn.scp scp:" + caminhoCorpus + "data/test/split3/1/feats.scp ark:- | " + caminhoSRC + "featbin/add-deltas  ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri1/decode/lat.1.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri1/graph/words.txt " + caminhoCorpus + "exp/tri1/final.mdl " + caminhoCorpus + "exp/tri1/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split3/2/utt2spk scp:" + caminhoCorpus + "data/test/split3/2/cmvn.scp scp:" + caminhoCorpus + "data/test/split3/2/feats.scp ark:- | " + caminhoSRC + "featbin/add-deltas  ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri1/decode/lat.2.gz'";
        system(command.toUtf8().constData());
        command = caminhoSRC + "gmmbin/gmm-latgen-faster --max-active=7000 --beam=13.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --allow-partial=true --word-symbol-table=" + caminhoCorpus + "exp/tri1/graph/words.txt " + caminhoCorpus + "exp/tri1/final.mdl " + caminhoCorpus + "exp/tri1/graph/HCLG.fst 'ark,s,cs:" + caminhoSRC + "featbin/apply-cmvn --utt2spk=ark:" + caminhoCorpus + "data/test/split3/3/utt2spk scp:" + caminhoCorpus + "data/test/split3/3/cmvn.scp scp:" + caminhoCorpus + "data/test/split3/3/feats.scp ark:- | " + caminhoSRC + "featbin/add-deltas  ark:- ark:- |' 'ark:|gzip -c >" + caminhoCorpus + "exp/tri1/decode/lat.3.gz'";
        system(command.toUtf8().constData());

        command = "rm -r " + caminhoCorpus + "exp/tri1/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());

        command = "cat " + caminhoCorpus + "exp/tri1/decode/scoring/17.tra | " + caminhoCorpus + "utils/int2sym.pl -f 2- " + caminhoCorpus + "data/lang/words.txt > " + caminhoCorpus + "exp/tri1/decode/scoring/test_output.txt";
        system(command.toUtf8().constData());
    }
}
