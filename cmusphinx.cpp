#include "cmusphinx.h"
#include <iostream>
#include <QDir>
#include <QMessageBox>

static QString local = QString("/home/") + getenv("USER") + ROOT_DIR;

CMUSphinx::CMUSphinx() {}

std::string CMUSphinx::test(int corpus, int configuracao) {
    QString corpusNome = "laps";
    if (corpus == 0) { //cbr
        corpusNome = "cbr";
    }
    ps_decoder_t *ps;
    cmd_ln_t *config;
    FILE *fh;
    char const *hyp;
    int16 buf[512];
    int rv;
    int32 score;

    config = cmd_ln_init(nullptr, ps_args(), TRUE,
             "-hmm", QString(local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/cmusphinx/") + corpusNome + QString("/model_parameters")).toUtf8().constData(),
             "-lm", QString(local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/cmusphinx/") + corpusNome + QString("/ocsr.lm.ARPA")).toUtf8().constData(),
             "-dict", QString(local+QString("utils/model/") + std::to_string(configuracao).c_str() + QString("/cmusphinx/") + corpusNome + QString("/ocsr.dic")).toUtf8().constData(),
             NULL);

    if (config == nullptr) {
        fprintf(stderr, "Failed to create config object, see log for details\n");
        return "";
    }

    ps = ps_init(config);
    if (ps == nullptr) {
        fprintf(stderr, "Failed to create recognizer, see log for details\n");
        return "";
    }
    std::string result = "";
    QFileInfoList list;
    QString arquivo = local + QString("utils/tests/") + corpusNome + QString("/corpus");
    QDir dir(arquivo.toUtf8().constData());
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Name);
    list = dir.entryInfoList();

    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        if (fileInfo.filePath().endsWith(".wav")) {
            fh = fopen(fileInfo.filePath().toUtf8().constData(), "rb");
            if (fh == nullptr) {
                fprintf(stderr, "Unable to open input file\n");
                return "";
            }
            rv = ps_start_utt(ps);
            while (!feof(fh)) {
                size_t nsamp;
                nsamp = fread(buf, 2, 512, fh);
                rv = ps_process_raw(ps, buf, nsamp, FALSE, FALSE);
            }
            rv = ps_end_utt(ps);
            hyp = ps_get_hyp(ps, &score);
            result += fileInfo.fileName().toUtf8().constData();
            result += " - ";
            result += hyp;
            result += "\n";
            fclose(fh);
        }
    }
    ps_free(ps);
    cmd_ln_free_r(config);

    return result;
}
